import image


class TestBuildkitProxyOptions:
    """
    Test image functions - buildkit_proxy_options
    """

    def test_variables_set_outputs_build_arg_options(self, monkeypatch):
        monkeypatch.setenv("HTTP_PROXY", "http://our.example")
        monkeypatch.setenv("HTTPS_PROXY", "https://our.example")
        monkeypatch.setenv("NO_PROXY", "http://foo.example")

        assert image.buildkit_proxy_options() == [
            "--opt",
            "build-arg:http_proxy=http://our.example",
            "--opt",
            "build-arg:https_proxy=https://our.example",
            "--opt",
            "build-arg:no_proxy=http://foo.example",
        ]

    def test_variables_not_set_outputs_nothing(self, monkeypatch):
        monkeypatch.delenv("HTTP_PROXY", raising=False)
        monkeypatch.delenv("HTTPS_PROXY", raising=False)
        monkeypatch.delenv("NO_PROXY", raising=False)

        assert image.buildkit_proxy_options() == []
