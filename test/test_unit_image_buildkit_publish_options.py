import pytest

import image


class TestBuildkitPublishOptions:
    """
    Test image functions - buildkit_publish_options
    """

    @pytest.fixture(autouse=True)
    def set_KOKKURI_REGISTRY_INTERNAL(self, monkeypatch):
        monkeypatch.setenv("KOKKURI_REGISTRY_INTERNAL", "our.example")

    def test_empty_if_given_no_name(self):
        assert image.buildkit_publish_options(None, None) == []
        assert image.buildkit_publish_options("", None) == []

    def test_given_at_least_a_name_outputs_publish_option(self):
        assert image.buildkit_publish_options("foo-name", None) == [
            "--output",
            'type=image,"name=our.example/foo-name",push=true',
        ]

    def test_given_a_name_and_tag_outputs_publish_option_with_both(self):
        assert image.buildkit_publish_options("foo-name", "foo-tag") == [
            "--output",
            'type=image,"name=our.example/foo-name:foo-tag",push=true',
        ]

    def test_given_a_name_tag_and_extra_tags_outputs_a_name_option_with_all_of_the_refs(
        self,
    ):
        assert image.buildkit_publish_options(
            "foo-name", "foo-tag", "extra1,extra2"
        ) == [
            "--output",
            'type=image,"name=our.example/foo-name:foo-tag,our.example/foo-name:extra1,our.example/foo-name:extra2",push=true',
        ]
