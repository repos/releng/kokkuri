import os
from unittest.mock import patch

import buildkit
import image


class TestBuildkitFrontend:
    """
    Test image functions - buildkit_frontend
    """

    def _write_blubber_file(self, tmpdir, contents):
        blubber_file = "blubber.yaml"

        with open(os.path.join(tmpdir, blubber_file), "w") as f:
            f.write(contents)

        return blubber_file

    @patch("image.KOKKURI_DEFAULT_BUILDKIT_FRONTEND", "our.example/frontend:v1.2.3")
    def test_with_no_user_frontend_uses_default_frontend(self, tmpdir):
        blubber_file = self._write_blubber_file(tmpdir, "version: v4\n")
        context = buildkit.Context("foo", str(tmpdir))

        assert (
            image.buildkit_frontend(context, blubber_file)
            == "our.example/frontend:v1.2.3"
        )

    @patch("image.KOKKURI_DEFAULT_BUILDKIT_FRONTEND", "our.example/frontend:v1.2.3")
    def test_respects_user_frontend(self, tmpdir):
        blubber_file = self._write_blubber_file(
            tmpdir,
            """# syntax=their.example/frontend:v0.0.1
build stuff
""",
        )
        context = buildkit.Context("foo", str(tmpdir))

        assert (
            image.buildkit_frontend(context, blubber_file)
            == "their.example/frontend:v0.0.1"
        )

    def test_dockerfile(self, tmpdir):
        file = "Dockerfile"
        context = buildkit.Context("foo", str(tmpdir))

        with open(os.path.join(tmpdir, file), "w") as f:
            f.write("FROM debian:11\n")

        assert image.buildkit_frontend(context, file) == image.DOCKERFILE_FRONTENDS[0]
