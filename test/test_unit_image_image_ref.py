import image


class TestUnitImageRef:
    """
    Test image functions - image ref functions
    """

    def test_image_ref(self):
        assert (
            image.image_ref("our.example/namespace", "foo-name", "foo-tag")
            == "our.example/namespace/foo-name:foo-tag"
        )

    def test_internal_image_ref(self, monkeypatch):
        monkeypatch.setenv("KOKKURI_REGISTRY_INTERNAL", "our.internal.example")
        assert (
            image.internal_image_ref("foo-name", "foo-tag")
            == "our.internal.example/foo-name:foo-tag"
        )

    def test_public_image_ref(self, monkeypatch):
        monkeypatch.setenv("KOKKURI_REGISTRY_PUBLIC", "our.public.example")
        assert (
            image.public_image_ref("foo-name", "foo-tag")
            == "our.public.example/foo-name:foo-tag"
        )

    def test_cache_image_ref(self, monkeypatch):
        monkeypatch.setenv("CI_PROJECT_PATH", "project/path")
        monkeypatch.setenv("KOKKURI_REGISTRY_CACHE", "our.cache.example")
        assert (
            image.cache_image_ref("foo-name", "foo-tag")
            == "our.cache.example/project/path/cache/foo-name:foo-tag"
        )

    def test_image_tag(self):
        assert image.image_tag("some disallowed/stuff") == "some-disallowed-stuff"
