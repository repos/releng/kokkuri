import json
import os

import image


class TestEnsureAuth:
    """
    Test image functions - ensure_auth
    """

    def test_ensure_auth(self, mock_docker_config, monkeypatch):
        monkeypatch.setenv("KOKKURI_JWT", "foo-jwt-token")

        monkeypatch.setenv("KOKKURI_REGISTRY_INTERNAL", "some.example")
        monkeypatch.setenv("KOKKURI_REGISTRY_CACHE", "another.example")

        monkeypatch.setenv("KOKKURI_REGISTRY_USER", "bruce")
        monkeypatch.setenv("KOKKURI_REGISTRY_PASSWORD", "secret")

        image.ensure_auth()

        configfile = os.path.join(os.getenv("DOCKER_CONFIG"), "config.json")

        with open(configfile) as f:
            got = json.load(f)

        assert got == {
            "auths": {
                "some.example": {
                    "auth": "YnJ1Y2U6c2VjcmV0",
                },
                "another.example": {"registrytoken": "foo-jwt-token"},
            }
        }

        monkeypatch.setenv("KOKKURI_REGISTRY_CACHE_USER", "sally")
        monkeypatch.setenv("KOKKURI_REGISTRY_CACHE_PASSWORD", "mustang")

        os.remove(configfile)
        image.ensure_auth()

        with open(configfile) as f:
            got = json.load(f)

        assert got == {
            "auths": {
                "some.example": {
                    "auth": "YnJ1Y2U6c2VjcmV0",
                },
                "another.example": {"auth": "c2FsbHk6bXVzdGFuZw=="},
            }
        }
