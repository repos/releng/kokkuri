import os
import pytest
import sys


sys.path.insert(0, os.path.normpath(os.path.join(__file__, "../../lib")))


@pytest.fixture
def mock_gitlab_job_variables(monkeypatch):
    monkeypatch.setenv("CI_COMMIT_REF_NAME", "foo-ref-name")
    monkeypatch.setenv("KOKKURI_JWT", "foo-jwt-token")
    monkeypatch.setenv("CI_JOB_NAME", "foo-job")


@pytest.fixture
def mock_gitlab_project_variables(monkeypatch):
    monkeypatch.setenv("CI_DEFAULT_BRANCH", "foo-default-branch")
    monkeypatch.setenv("CI_PROJECT_PATH", "foo/project")


@pytest.fixture
def mock_kokkuri_registry_variables(monkeypatch):
    monkeypatch.setenv("KOKKURI_REGISTRY_INTERNAL", "registry.local.example")
    monkeypatch.setenv("KOKKURI_REGISTRY_PUBLIC", "registry.example")
    monkeypatch.setenv("KOKKURI_REGISTRY_CACHE", "registry.cache.example")


@pytest.fixture
def mock_docker_config(tmpdir, monkeypatch):
    monkeypatch.setenv("DOCKER_CONFIG", str(tmpdir))


@pytest.fixture
def mock_gitlab_environment(
    mock_gitlab_job_variables,
    mock_gitlab_project_variables,
    mock_kokkuri_registry_variables,
    mock_docker_config,
):
    pass


@pytest.fixture
def mock_gitlab_mr_variables(monkeypatch):
    monkeypatch.setenv("CI_MERGE_REQUEST_ID", "foo-mr-123")
    monkeypatch.setenv("CI_MERGE_REQUEST_SOURCE_BRANCH_NAME", "foo-mr-source-branch")
    monkeypatch.setenv("CI_MERGE_REQUEST_TARGET_BRANCH_NAME", "foo-mr-target-branch")
