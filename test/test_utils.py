import utils


def test_parse_image_name():
    test_cases = [
        ["repo/image", (None, "repo/image", None)],
        ["repo/image:v1", (None, "repo/image", "v1")],
        ["registry.net/repo/image", ("registry.net", "repo/image", None)],
        ["registry.net/repo/image:v1", ("registry.net", "repo/image", "v1")],
        ["registry.net:5000/repo/image", ("registry.net:5000", "repo/image", None)],
        ["registry.net:5000/repo/image:v1", ("registry.net:5000", "repo/image", "v1")],
    ]

    for image_name, expected in test_cases:
        got = utils.parse_image_name(image_name)
        assert got == expected


def test_tagless_image_name():
    test_cases = [
        ["repo/image", "repo/image"],
        ["repo/image:v1", "repo/image"],
        ["registry.net/repo/image", "registry.net/repo/image"],
        ["registry.net/repo/image:v1", "registry.net/repo/image"],
        ["registry.net:5000/repo/image", "registry.net:5000/repo/image"],
        ["registry.net:5000/repo/image:v1", "registry.net:5000/repo/image"],
    ]

    for image_name, expected in test_cases:
        got = utils.tagless_image_name(image_name)
        assert got == expected
