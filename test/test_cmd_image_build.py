import os
import shlex
import subprocess
import utils

import pytest


class TestCmdImageBuild:
    """
    Test `kokkuri image build`
    """

    @pytest.fixture
    def buildctl_stub(self, monkeypatch, tmpdir):
        buildctl_stub = os.path.join(tmpdir, "buildctl")

        with open(buildctl_stub, "w") as f:
            f.write(
                """#!/bin/bash
echo buildctl "$@"
"""
            )
        os.chmod(buildctl_stub, 0o755)

        monkeypatch.setenv("PATH", str(tmpdir), prepend=os.pathsep)

    @pytest.fixture
    def fixtures(self, buildctl_stub, mock_gitlab_environment, monkeypatch):
        monkeypatch.setenv("BUILD_VARIANT", "foo")

    def test_on_branch_job_no_caching(self, fixtures, monkeypatch):
        monkeypatch.delenv("KOKKURI_REGISTRY_CACHE")
        res = subprocess.run(
            ["bin/kokkuri", "image", "build"],
            check=True,
            capture_output=True,
            text=True,
        )

        assert (
            res.stdout
            == "buildctl build --progress=plain --frontend=gateway.v0 --opt source=docker-registry.wikimedia.org/repos/releng/blubber/buildkit:v1.0.1 --opt filename=.pipeline/blubber.yaml --opt target=foo --local context=. --local dockerfile=.\n"
        )

    def test_on_branch_job_with_caching(self, fixtures):
        res = subprocess.run(
            ["bin/kokkuri", "image", "build"],
            check=True,
            capture_output=True,
            text=True,
        )

        assert (
            res.stdout
            == "buildctl build --progress=plain --frontend=gateway.v0 --opt source=docker-registry.wikimedia.org/repos/releng/blubber/buildkit:v1.0.1 --opt filename=.pipeline/blubber.yaml --opt target=foo --local context=. --local dockerfile=. --import-cache type=registry,ref=registry.cache.example/foo/project/cache/foo:foo-default-branch --import-cache type=registry,ref=registry.cache.example/foo/project/cache/foo:foo-ref-name --export-cache type=registry,ref=registry.cache.example/foo/project/cache/foo:foo-ref-name\n"
        )

    def test_on_merge_request_branch_with_caching(
        self, fixtures, mock_gitlab_mr_variables
    ):
        res = subprocess.run(
            ["bin/kokkuri", "image", "build"],
            check=True,
            capture_output=True,
            text=True,
        )

        assert (
            res.stdout
            == "buildctl build --progress=plain --frontend=gateway.v0 --opt source=docker-registry.wikimedia.org/repos/releng/blubber/buildkit:v1.0.1 --opt filename=.pipeline/blubber.yaml --opt target=foo --local context=. --local dockerfile=. --import-cache type=registry,ref=registry.cache.example/foo/project/cache/foo:foo-mr-target-branch --import-cache type=registry,ref=registry.cache.example/foo/project/cache/foo:foo-mr-source-branch --export-cache type=registry,ref=registry.cache.example/foo/project/cache/foo:foo-mr-source-branch\n"
        )

    def test_accepts_extra_buildctl_options(self, fixtures, monkeypatch):
        monkeypatch.delenv("KOKKURI_REGISTRY_CACHE")
        res = subprocess.run(
            ["bin/kokkuri", "image", "build", "--", "--opt", "some=foo"],
            check=True,
            capture_output=True,
            text=True,
        )

        assert (
            res.stdout
            == "buildctl build --progress=plain --frontend=gateway.v0 --opt source=docker-registry.wikimedia.org/repos/releng/blubber/buildkit:v1.0.1 --opt filename=.pipeline/blubber.yaml --opt target=foo --local context=. --local dockerfile=. --opt some=foo\n"
        )

    def test_publish_provenance_set_to_no_omits_attestation_option(
        self, fixtures, monkeypatch
    ):
        monkeypatch.setenv("PUBLISH_PROVENANCE", "no")
        res = subprocess.run(
            ["bin/kokkuri", "image", "build"],
            check=True,
            capture_output=True,
            text=True,
        )

        assert "attest:provenance" not in res.stdout

    def test_publish_provenance_set_to_yes_includes_attestation_option(
        self, fixtures, monkeypatch
    ):
        monkeypatch.setenv("PUBLISH_PROVENANCE", "yes")
        res = subprocess.run(
            ["bin/kokkuri", "image", "build"],
            check=True,
            capture_output=True,
            text=True,
        )

        assert ("--opt", "attest:provenance=mode=max") in utils.pairwise(
            shlex.split(res.stdout)
        )
