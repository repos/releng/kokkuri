# Test image functions - buildkit_caching_options

import image


class TestBuildkitCachingOptions:
    def test_empty_if_no_cache_registry(self):
        assert image.buildkit_caching_options("foo-variant") == []

    def test_with_cache_registry_on_branch_or_tag(self, mock_gitlab_environment):
        assert image.buildkit_caching_options("foo-variant") == [
            "--import-cache",
            "type=registry,ref=registry.cache.example/foo/project/cache/foo-variant:foo-default-branch",
            "--import-cache",
            "type=registry,ref=registry.cache.example/foo/project/cache/foo-variant:foo-ref-name",
            "--export-cache",
            "type=registry,ref=registry.cache.example/foo/project/cache/foo-variant:foo-ref-name",
        ]

    def test_with_cache_registry_on_merge_request_branch(
        self, mock_gitlab_environment, mock_gitlab_mr_variables
    ):
        assert image.buildkit_caching_options("foo-variant") == [
            "--import-cache",
            "type=registry,ref=registry.cache.example/foo/project/cache/foo-variant:foo-mr-target-branch",
            "--import-cache",
            "type=registry,ref=registry.cache.example/foo/project/cache/foo-variant:foo-mr-source-branch",
            "--export-cache",
            "type=registry,ref=registry.cache.example/foo/project/cache/foo-variant:foo-mr-source-branch",
        ]
