# Test image functions - buildkit_context
import base64
import pytest
import requests_mock

import buildkit


@pytest.fixture
def gitlab_ci(monkeypatch):
    monkeypatch.setenv("CI_SERVER_URL", "https://gitlab.example/")
    monkeypatch.setenv("CI_JOB_TOKEN", "abc123")


@pytest.fixture
def gitlab_project_file(gitlab_ci):
    content = "foo-content"

    with requests_mock.Mocker() as mock:
        mock.get(
            "https://gitlab.example/api/v4/projects/repos%2Ffoo",
            headers={"content-type": "application/json"},
            json={"id": 1},
        )
        mock.get(
            "https://gitlab.example/api/v4/projects/1/repository/files/foo?ref=main",
            headers={"content-type": "application/json"},
            json={
                "encoding": "base64",
                "content": base64.b64encode(content.encode("utf-8")).decode("utf-8"),
            },
        )
        yield


class TestBuildkitContext:
    def test_buildctl_options_uses_local_if_local(self):
        context = buildkit.Context("foo", "/some/local/dir")
        assert context.buildctl_options() == ["--local", "foo=/some/local/dir"]

    def test_buildctl_options_uses_opt_if_remote(self):
        context = buildkit.Context("foo", "https://remote.example")
        assert context.buildctl_options() == ["--opt", "foo=https://remote.example"]

    def test_safe_url_removes_usernames_and_passwords(self):
        context = buildkit.Context("foo", "https://un:pw@remote.example")
        assert context.safe_url == "https://remote.example"

    def test_remote_read_file(self, gitlab_project_file):
        url = "https://gitlab.example/repos/foo.git#main"
        context = buildkit.Context("foo", url)
        assert context.read_file("foo") == "foo-content"
