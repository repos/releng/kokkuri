import os

import image


class TestUnitImageCommon:
    """
    Test common functions
    """

    def test_export_job_variable(self, tmpdir, monkeypatch, mock_gitlab_environment):
        kokkuri_dotenv_path = os.path.join(tmpdir, ".kokkuri.env")

        monkeypatch.setenv("KOKKURI_DOTENV_PATH", kokkuri_dotenv_path)

        image.export_job_variable("FOO", "foo value")
        image.export_job_variable("BAR", "bar value")

        with open(kokkuri_dotenv_path) as f:
            assert f.read() == "FOO_JOB_FOO=foo value\nFOO_JOB_BAR=bar value\n"

    def test_looks_like_a_dockerfile(self):
        assert image.looks_like_a_dockerfile("dockerfile", "")
        assert image.looks_like_a_dockerfile("Dockerfile", "")
        assert image.looks_like_a_dockerfile("dockerfile.x", "")
        assert image.looks_like_a_dockerfile("Dockerfile.x", "")
        assert image.looks_like_a_dockerfile("some_file", "FROM debian:11\n")
        assert image.looks_like_a_dockerfile("some_file", " from debian:11\n")
        assert not image.looks_like_a_dockerfile("some_file", "foo\n")
