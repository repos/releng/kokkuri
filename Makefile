#BUILD_QUIET = -q

.PHONY: lint test

lint test:
	DOCKER_BUILDKIT=1 docker build $(BUILD_QUIET) -f .pipeline/blubber.yaml --target $@ --tag localhost:5000/kokkuri/$@ .
	docker run --rm -it localhost:5000/kokkuri/$@

_lint:
	tox -e lint --workdir /tmp/.tox

_test:
	tox -e test --workdir /tmp/.tox -- -vv

fix:
	DOCKER_BUILDKIT=1 docker build $(BUILD_QUIET) -f .pipeline/blubber.yaml --target fix --tag localhost:5000/kokkuri/fix .
	docker run --rm -it -u $(shell id -u):$(shell id -g) -v $(shell pwd):/source:rw -w /source localhost:5000/kokkuri/fix --in-place $(shell git ls-files '*.py')

image:
	DOCKER_BUILDKIT=1 docker build $(BUILD_QUIET) -f .pipeline/blubber.yaml --target kokkuri -t kokkuri .

run: image
	docker run --rm -it kokkuri
