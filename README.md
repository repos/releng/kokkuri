![kokkuri](docs/logo-400.png)

Summon our tricksy CI functions to build/test/publish your WMF deployable artifacts.

## Getting started

This project contains reusable GitLab CI templates and includes that are meant
to help you get your deployable artifacts built, tested, and published for WMF
production deployment.

## Usage

Kokkuri can be used in GitLab CI as either an [include][gitlab_include] or a
[component][gitlab_component]. The examples below use a component style
include.

## Examples

### Build an image variant

Build an image variant from your [Blubber][blubber] config by including
[includes/images.yaml](./includes/images.yaml) and extending
`.kokkuri:build-image`.

```yaml
include:
  - component: 'gitlab.wikimedia.org/repos/releng/kokkuri/images@<version>'

stages:
  - build

build-my-test-variant:
  extends: .kokkuri:build-image
  stage: build
  variables:
    BUILD_VARIANT: test
```

### Run an image variant

Build and run an image variant (such as a test suite) from your
[Blubber][blubber] config using `.kokkuri:build-and-run-image`.

```yaml
include:
  - component: 'gitlab.wikimedia.org/repos/releng/kokkuri/images@<version>'

stages:
  - test

run-my-test-variant:
  extends: .kokkuri:build-and-run-image
  stage: test
  variables:
    BUILD_VARIANT: npm-run
    RUN_ARGUMENTS: '["test", "--", "specific.test.js"]'
```

### Publish an image variant

Build and publish a production image variant using
`.kokkuri:build-and-publish-image`.

Typically you'd want to do this each time a protected branch receives a merge
or when a new protected tag is pushed, and by default the image will be tagged
with the git branch/tag.

```yaml
include:
  - component: 'gitlab.wikimedia.org/repos/releng/kokkuri/images@<version>'

stages:
  - publish

publish-my-production-variant:
  extends: .kokkuri:build-and-publish-image
  stage: publish
  variables:
    BUILD_VARIANT: production
  tags:
    - trusted
  rules:
    - if: $CI_COMMIT_TAG && $CI_COMMIT_REF_PROTECTED
```

#### Publishing image provenance

Users of Blubber (>= v1.0.0) can now easily generate provenance metadata and
have it published to the registry alongside their image. With Kokkuri, it's as
easy as setting `PUBLISH_PROVENANCE: 'yes'`.

To get the most accurate and complete provenance information, you'll also want
to use the mixin `.kokkuri:remote-context` whenever possible. This instructs
BuildKit to fetch your repo/ref directly from GitLab instead of transferring
the local build directory.

```yaml
publish-my-production-variant:
  extends: [.kokkuri:build-and-publish-image, .kokkuri:remote-context] # <-
  stage: publish
  variables:
    BUILD_VARIANT: production
    PUBLISH_PROVENANCE: 'yes' # <-
  tags:
    - trusted
  rules:
    - if: $CI_COMMIT_TAG && $CI_COMMIT_REF_PROTECTED
```

See [Blubber's documentation on image attestations](https://gitlab.wikimedia.org/repos/releng/blubber#image-attestations)
for details on how provenance is generated and stored in the registry.

#### Using published images in downstream jobs

A `.kokkuri:build-and-publish-image` job sets two new environment variables
for use by downstream jobs. The names of both are prefixed by the
publish-image job name (with non-`[A-Z0-9]` characters converted to
underscore).

 - `${CI_JOB_NAME}_IMAGE_REF` - A public ref to the published image.
 - `${CI_JOB_NAME}_IMAGE_INTERNAL_REF` - An internal (i.e. `.wmnet` host) ref
   of the published image. (Note this is only distinguishable from the public
   ref on trusted runners and may become deprecated soon as it is of marginal
   use.)

You can use these variables to achieve some useful CI patterns. For example:

#### Build once, run many times

It's wasteful and slow to install your test dependencies over and over again
for slightly different images. Image layers may be cached by buildkitd on its
local filesystem but there's no guarantee that your build will land on the
same buildkitd the next time around.

Using a build-once-run-many pattern, you can install your test dependencies
into one image, and reuse that image in subsequent jobs to run your tests
and/or linters, even in parallel.

```yaml
stages:
 - test

test-runner:
  stage: test
  extends: .kokkuri:build-and-publish-image
  variables:
    BUILD_VARIANT: test-runner
    PUBLISH_IMAGE_TAG: job-${CI_JOB_ID} # you'll want something distinct here

run-tests:
  stage: test
  needs: [test-runner] # you could use a subsequent stage but this is nicer
  image: "${TEST_RUNNER_IMAGE_REF}" # use the image built by `test-runner`
  script: # note you must provide a `script` as GitLab overrides the entrypoint
    - make test
  parallel: # matrix as you like without the extra build overhead
    matrix:
      - TEST_SET: ["@example-set1", "@example-set2"]
```

#### Run acceptance or integration tests against your built image

We're testing Blubber with this pattern, for example, building two published
images—an acceptance test runner and a version of Blubber with changes
applied—prior to reusing them in a subsequent job that that executes one
against the other.

```yaml
stages:
  - test
  - acceptance

frontend-for-testing:
  stage: test
  extends: .kokkuri:build-and-publish-image
  variables:
    BUILD_VARIANT: buildkit
    BUILD_TARGET_PLATFORMS: linux/amd64
    PUBLISH_IMAGE_TAG: job-${CI_JOB_ID}

acceptance-runner:
  stage: test
  extends: .kokkuri:build-and-publish-image
  variables:
    BUILD_VARIANT: acceptance
    PUBLISH_IMAGE_TAG: job-${CI_JOB_ID}

run-acceptance-tests:
  stage: acceptance
  image: "${ACCEPTANCE_RUNNER_IMAGE_REF}"
  variables:
    BLUBBER_TEST_IMAGE: "${FRONTEND_FOR_TESTING_IMAGE_REF}"
  script:
    - make examples
  parallel:
    matrix:
      - BLUBBER_ONLY_EXAMPLES: ["@set1", "@set2", "@set3", "@set4"]
```

#### Run your built image as a GitLab CI service

You can use `${{JOB_NAME}_IMAGE_REF}` [anywhere GitLab CI supports
variables][gitlab_variables], including in the `services` section of your
jobs. Use this pattern to spin up a service container using your built image
to, for example, run some integration tests.

```yaml
integration-runner:
  extends: .kokkuri:build-and-publish-image
  stage: test
  variables:
    BUILD_VARIANT: integration-tester
    PUBLISH_IMAGE_TAG: job-${CI_JOB_ID}

app-with-changes:
  extends: .kokkuri:build-and-publish-image
  stage: test
  variables:
    BUILD_VARIANT: app
    PUBLISH_IMAGE_TAG: job-${CI_JOB_ID}

integration-tests:
  stage: test
  needs: [integration-runner, app-with-changes]
  image: "${INTEGRATION_RUNNER_IMAGE_REF}"
  script:
    - run integration-tests --addr tcp://app:1234
  services:
    - name: "${APP_WITH_CHANGES_IMAGE_REF}"
      alias: app
      command: [start]
```

[blubber]: https://wikitech.wikimedia.org/wiki/Blubber
[gitlab_variables]: https://docs.gitlab.com/ee/ci/variables/where_variables_can_be_used.html
[gitlab_component]: https://docs.gitlab.com/ci/components/
[gitlab_include]: https://docs.gitlab.com/ci/yaml/includes/
