import gitlab
import os
from urllib.parse import urlparse

import gitlab_ci


class Context:
    """
    Represents a parsed BuildKit context from a remote URL or local file path.
    """

    def __init__(self, name: str, url: str):
        self.name = name
        self.url = urlparse(url)

    def is_same_server(self, url: str) -> bool:
        parsed_url = urlparse(url)
        return (
            parsed_url.scheme == self.url.scheme
            and parsed_url.hostname == self.url.hostname
            and parsed_url.port == self.url.port
        )

    def read_file(self, path) -> str:
        """
        Reads file contents from context.

        If this is a remote context hosted on GitLab, an attempt is made to
        fetch the file contents via the API.
        """
        if self.is_local():
            path = self.path(path)
            if not os.path.exists(path):
                return None
            with open(path, "r") as f:
                return f.read()

        ref = self.url.fragment
        if ref == "":
            return None

        # If the remote context is hosted on the GitLab server, attempt to
        # read the file via the API
        gl = gitlab_ci.client()
        if gl is not None and self.is_same_server(gl.url):
            try:
                project_path = self.url.path.removeprefix("/")
                project_path = project_path.removesuffix(".git")
                project = gl.projects.get(project_path)
                file = project.files.get(file_path=path, ref=ref)
                return file.decode().decode("utf-8")
            except gitlab.GitlabGetError:
                pass

        return None

    def is_local(self) -> bool:
        return self.url.netloc == "" and self.url.scheme in ["", "file"]

    @property
    def safe_url(self) -> str:
        """
        The URL string of this context with any embedded usernames and
        passwords removed.
        """

        # Remove auth by reconstructing netloc without username and password
        netloc = "" if self.url.hostname is None else self.url.hostname

        if self.url.port is not None:
            netloc += f":{self.url.port}"

        return self.url._replace(netloc=netloc).geturl()

    def path(self, relpath) -> str:
        """
        Returns a new path from one relative to the context path.

        Note this is only useful for local contexts which can be determined
        with :func:`~is_local`.
        """

        return os.path.join(self.url.path, relpath)

    def buildctl_options(self) -> list:
        """
        Returns the correct buildctl build options depending on whether the given
        context is remote or local.
        """

        return [
            "--local" if self.is_local() else "--opt",
            f"{self.name}={self.safe_url}",
        ]
