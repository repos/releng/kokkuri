import base64
import click
import json
import os
import re
import shlex
import subprocess
import tempfile
from typing import Optional
import uuid

import buildkit
from utils import warning, info, debug, error

KOKKURI_DEFAULT_BUILDKIT_FRONTEND = (
    "docker-registry.wikimedia.org/repos/releng/blubber/buildkit:v0.23.0"
)
DOCKERFILE_FRONTENDS = ["docker/dockerfile:latest", "docker/dockerfile-upstream:master"]


@click.group()
def image():
    """
    Image management commands
    """


@image.command()
@click.argument("buildctl-arguments", nargs=-1)
@click.option("--variant", envvar="BUILD_VARIANT", help="The image variant to build")
@click.option(
    "--context",
    default=".",
    envvar="BUILD_CONTEXT",
    help="Build directory sent to buildkit",
    show_default=True,
)
@click.option(
    "--config-context",
    default=".",
    envvar="BUILD_CONFIG_CONTEXT",
    help="Config directory sent to buildkit",
    show_default=True,
)
@click.option(
    "--config",
    default=".pipeline/blubber.yaml",
    envvar="BUILD_CONFIG",
    help="Build config",
    show_default=True,
)
@click.option(
    "--target-platforms",
    default=None,
    envvar="BUILD_TARGET_PLATFORMS",
    help="Target build platforms (e.g. 'linux/amd64')",
)
@click.option(
    "--frontend",
    default=None,
    envvar="BUILD_FRONTEND",
    help="The BuildKit frontend to use when building the image",
)
@click.option(
    "--publish-image-name",
    default=None,
    envvar="PUBLISH_IMAGE_NAME",
    help="Name under which to publish the image",
)
@click.option(
    "--publish-image-tag",
    default=None,
    envvar="PUBLISH_IMAGE_TAG",
    help="Tag under which to publish the image",
)
@click.option(
    "--publish-image-extra-tags",
    default=None,
    envvar="PUBLISH_IMAGE_EXTRA_TAGS",
    help="Extra tags to publish for the image (comma separated)",
)
@click.option(
    "--publish-provenance/--no-publish-provenance",
    default=False,
    envvar="PUBLISH_PROVENANCE",
    help="Publish provenance metadata",
)
def build(
    buildctl_arguments,
    variant,
    context,
    config_context,
    config,
    target_platforms=None,
    frontend=None,
    publish_image_name=None,
    publish_image_tag=None,
    publish_image_extra_tags="",
    publish_provenance=False,
):
    """
    Build the specified VARIANT
    """

    canonicalize_environment_variables()

    context = buildkit.Context("context", context)
    config_context = buildkit.Context("dockerfile", config_context)

    if frontend is None:
        frontend = buildkit_frontend(config_context, config)

    if not variant:
        if frontend in DOCKERFILE_FRONTENDS:
            variant = ""
        else:
            raise SystemExit(
                "The variant must be set using --variant or the BUILD_VARIANT environment variable"
            )

    publish_options = []

    if publish_image_name:
        if not public_registry():
            raise SystemExit("KOKKURI_REGISTRY_PUBLIC must be set to publish an image")

        publish_options += buildkit_publish_options(
            publish_image_name, publish_image_tag, publish_image_extra_tags
        )

    if publish_provenance:
        publish_options += ["--opt", "attest:provenance=mode=max"]

    ensure_auth()

    platform_opts = (
        ["--opt", f"platform={target_platforms}"] if target_platforms else []
    )

    cmd = (
        ["buildctl"]
        + shlex.split(os.getenv("BUILDCTL_FLAGS", ""))
        + [
            "build",
            "--progress=plain",
            "--frontend=gateway.v0",
            "--opt",
            f"source={frontend}",
            "--opt",
            f"filename={config}",
            "--opt",
            f"target={variant}",
        ]
        + context.buildctl_options()
        + config_context.buildctl_options()
        + platform_opts
        + buildkit_proxy_options()
        + buildkit_caching_options(variant)
        + publish_options
        + shlex.split(os.getenv("BUILDCTL_BUILD_FLAGS", ""))
        + list(buildctl_arguments)
    )

    debug("Running %s", cmd)
    try:
        subprocess.run(cmd, check=True)
    except subprocess.CalledProcessError as e:
        error("%s", e)
        raise SystemExit(1)

    if publish_image_name:
        export_job_variable("IMAGE_TAG", publish_image_tag)
        export_job_variable(
            "IMAGE_INTERNAL_REF",
            internal_image_ref(publish_image_name, publish_image_tag),
        )
        export_job_variable(
            "IMAGE_REF", public_image_ref(publish_image_name, publish_image_tag)
        )


def buildkit_caching_options(variant) -> list:
    """
    Check for a caching registry and import/export caches based on the
    repo/branch/variant. Runners in environment that have a caching registry
    should define KOKKURI_REGISTRY_CACHE as the registry host/port.

    Import/export refs will be guessed in a heuristic fashion based on CI
    variables to hopefully maximize the possibility of common cache lineage
    between this job/pipeline and previous ones.

    When dealing with a merge request, we'll both push and pull using a ref
    based on the MR source branch, and pull using a ref based on the target
    branch.

    When dealing with a branch or tag, we'll both push and pull using a ref
    based on the git ref name, and pull using a ref based on the repos default
    branch.
    """
    res = []

    import_refs = []

    if not variant:
        variant = "default"

    if cache_registry():
        info("Registry based caching available at %s", kokkuri_registry_cache_ns())

        if os.getenv("CI_MERGE_REQUEST_ID"):
            import_refs.append(
                cache_image_ref(
                    variant, os.getenv("CI_MERGE_REQUEST_TARGET_BRANCH_NAME")
                )
            )
            import_refs.append(
                cache_image_ref(
                    variant, os.getenv("CI_MERGE_REQUEST_SOURCE_BRANCH_NAME")
                )
            )
            export_ref = cache_image_ref(
                variant, os.getenv("CI_MERGE_REQUEST_SOURCE_BRANCH_NAME")
            )
        else:
            import_refs.append(cache_image_ref(variant, os.getenv("CI_DEFAULT_BRANCH")))
            import_refs.append(
                cache_image_ref(variant, os.getenv("CI_COMMIT_REF_NAME"))
            )
            export_ref = cache_image_ref(variant, os.getenv("CI_COMMIT_REF_NAME"))

        for ref in import_refs:
            res.extend(["--import-cache", f"type=registry,ref={ref}"])

        res.extend(["--export-cache", f"type=registry,ref={export_ref}"])

    return res


def export_job_variable(name, value):
    """
    Saves a GitLab dotenv report for the given variable and value. The report
    can then be saved as an artifact and imported by subsequent stages where the
    variables may be utilized within job fields that support variable expansion.

    Note that the variable name will be prefixed by the name of the current job.

    Example (in job FOO_JOB):

    export_job_variable("KEY", "value")
    cat .kokkuri.env # FOO_JOB_KEY=value
    """

    prefix = re.sub(r"[^A-Z0-9]", "_", os.getenv("CI_JOB_NAME").upper())
    fullname = f"{prefix}_{name}"

    kokkuri_dotenv_path = os.getenv("KOKKURI_DOTENV_PATH", ".kokkuri.env")

    debug(
        "Saving variable '%s' with value '%s' to dotenv report %s",
        fullname,
        value,
        kokkuri_dotenv_path,
    )

    with open(kokkuri_dotenv_path, "a") as f:
        f.write(f"{fullname}={value}\n")


def buildkit_publish_options(name, tag, extra_tags="") -> list:
    if not name:
        return []

    tags = [tag]
    if extra_tags:
        tags.extend(re.split("[, ]", extra_tags))

    refs = []

    for tag in tags:
        refs.append(internal_image_ref(name, tag))

    refs = ",".join(refs)

    return ["--output", f'type=image,"name={refs}",push=true']


def buildkit_proxy_options():
    res = []

    for var in ["http_proxy", "https_proxy", "no_proxy"]:
        var_upcase = var.upper()
        value = os.getenv(var_upcase)
        if value:
            debug("%s set to %s", var_upcase, value)
            res.extend(["--opt", f"build-arg:{var}={value}"])

    return res


def buildkit_frontend(context: buildkit.Context, config_path: str) -> str:
    """
    Parse syntax= lines from the image build config context.
    """

    frontend = KOKKURI_DEFAULT_BUILDKIT_FRONTEND
    user_frontend = None

    config = context.read_file(config_path)
    if config is not None:
        for line in config.splitlines():
            m = re.match(r"^# *syntax *= *(\S+)", line)
            if m:
                user_frontend = m[1]
            else:
                break

        if not user_frontend and looks_like_a_dockerfile(config_path, config):
            info("Using Dockerfile build frontend")
            return DOCKERFILE_FRONTENDS[0]

    if user_frontend:
        info("Valid syntax line found in %s", config_path)
        frontend = user_frontend

    info("Using build frontend %s", frontend)
    return frontend


def looks_like_a_dockerfile(path, content) -> bool:
    if re.match(r"Dockerfile", os.path.basename(path), re.IGNORECASE):
        return True

    for line in content.splitlines():
        # "A Dockerfile must begin with a FROM instruction."
        # xref https://docs.docker.com/engine/reference/builder/
        # We loosen that rule here and say that a Dockerfile must contain
        # a FROM instruction.
        if re.match(r"\s*from\s+", line, re.IGNORECASE):
            return True

    return False


def image_tag(string) -> str:
    """
    Convert 'string' to a string that is suitable for use as an image tag.
    """
    # A tag name must be valid ASCII and may contain lowercase and
    # uppercase letters, digits, underscores, periods and dashes. A
    # tag name may not start with a period or a dash and may contain a
    # maximum of 128 characters.
    # xref https://docs.docker.com/engine/reference/commandline/tag/#description
    return re.sub(r"[^a-zA-Z0-9_.-]", "-", string)[:128]


def image_ref(namespace, name, tag) -> str:
    res = os.path.join(namespace, name)

    if tag:
        res = res + ":" + image_tag(tag)

    return res


def project_registry_path(registry, *subpaths) -> str:
    """
    Returns REGISTRY/CI_PROJECT_PATH{/subpaths/...}
    """
    return os.path.join(registry, os.getenv("CI_PROJECT_PATH", ""), *subpaths)


def kokkuri_registry_cache_ns() -> str:
    return project_registry_path(cache_registry(), "cache")


REGISTRY_INFO = {
    "public": {
        "var": "KOKKURI_REGISTRY_PUBLIC",
    },
    "push": {
        "var": "KOKKURI_REGISTRY_INTERNAL",
        "user_var": "KOKKURI_REGISTRY_USER",
        "password_var": "KOKKURI_REGISTRY_PASSWORD",
    },
    "cache": {
        "var": "KOKKURI_REGISTRY_CACHE",
        "user_var": "KOKKURI_REGISTRY_CACHE_USER",
        "password_var": "KOKKURI_REGISTRY_CACHE_PASSWORD",
    },
}


def canonicalize_environment_variables():
    if os.getenv("KOKKURI_REGISTRY_PUBLIC") and not os.getenv(
        "KOKKURI_REGISTRY_INTERNAL"
    ):
        os.environ["KOKKURI_REGISTRY_INTERNAL"] = os.getenv("KOKKURI_REGISTRY_PUBLIC")


def public_registry() -> Optional[str]:
    return os.getenv(REGISTRY_INFO["public"]["var"])


def push_registry() -> Optional[str]:
    return os.getenv(REGISTRY_INFO["push"]["var"])


def cache_registry() -> Optional[str]:
    return os.getenv(REGISTRY_INFO["cache"]["var"])


def get_registry_info(type):
    info = REGISTRY_INFO[type]
    return (
        os.getenv(info["var"]),
        os.getenv(info["user_var"]),
        os.getenv(info["password_var"]),
    )


def cache_image_ref(name, tag) -> str:
    return image_ref(kokkuri_registry_cache_ns(), name, tag)


def internal_image_ref(name, tag) -> str:
    return image_ref(push_registry(), name, tag)


def public_image_ref(name, tag) -> str:
    return image_ref(public_registry(), name, tag)


def string_to_base64_string(input: str) -> str:
    """
    Returns a string representing the base64 encoding of the input string.
    """
    return base64.b64encode(input.encode("utf-8")).decode("utf-8")


def ensure_auth() -> None:
    jwt = os.getenv("KOKKURI_JWT")

    docker_config_dir = os.getenv(
        "DOCKER_CONFIG", os.path.join(os.getenv("HOME", ""), ".docker")
    )
    config_file = os.path.join(docker_config_dir, "config.json")

    os.makedirs(docker_config_dir, exist_ok=True)

    config = {}

    if os.path.exists(config_file):
        with open(config_file) as f:
            config = json.load(f)

    config.setdefault("auths", {})

    for type in ["push", "cache"]:
        registry, username, password = get_registry_info(type)
        if not registry:
            continue

        if username and password:
            info(
                "Configuring Docker client to use username/password auth for %s",
                registry,
            )
            config["auths"][registry] = {
                "auth": string_to_base64_string(f"{username}:{password}")
            }
        elif jwt and not config["auths"].get(registry):
            info("Configuring Docker client to use JWT auth for %s", registry)
            config["auths"][registry] = {"registrytoken": jwt}

    with open(config_file, "w") as f:
        json.dump(config, f, indent=4)


@image.command()
@click.argument("chart")
@click.argument("image")
@click.argument("image_tag")
@click.option(
    "--test/--notest", default=False, help="Run 'helm test' after installing the chart"
)
def deploy(chart, image, image_tag, test=False):
    """
    Test deployment of CHART in the 'ci' namespace in WikiKube
    staging, using IMAGE:IMAGE_TAG as the image to run.

    CHART must be the name of a chart in the Wikimedia charts repository.
    IMAGE must be an image name relative to docker-registry.wikimedia.org.
    """
    helmTimeout = "120s"
    chartRepository = "https://helm-charts.wikimedia.org/stable/"
    namespace = "ci"
    registry = "docker-registry.wikimedia.org"
    pullPolicy = "Always"
    version = ""

    release = str(uuid.uuid4())

    with tempfile.NamedTemporaryFile("w") as values:
        values.write(
            f"""
docker:
  registry: {registry}
  pull_policy: {pullPolicy}
main_app:
  image: {image}
  version: {image_tag}
"""
        )
        values.flush()

        try:
            run_helm(
                [
                    "helm",
                    "install",
                    release,
                    chart,
                    "--namespace",
                    namespace,
                    "--values",
                    values.name,
                    "--debug",
                    "--wait",
                    "--timeout",
                    helmTimeout,
                    "--repo",
                    chartRepository,
                    version,
                ]
            )

            if test:
                run_helm(["helm", "test", release])

        finally:
            try:
                run_helm(["helm", "delete", release])
            except Exception as e:
                warning("helm delete failed: %s", e)


def run_helm(args: list):
    subprocess.run(
        args, check=True, env={"KUBECONFIG": os.getenv("CI_STAGING_KUBECONFIG")}
    )
