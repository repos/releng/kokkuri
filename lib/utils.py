import logging
import re


def debug(format, *args):
    logging.getLogger().debug(format, *args)


def info(format, *args):
    logging.getLogger().info(format, *args)


def warning(format, *args):
    logging.getLogger().warning(format, *args)


def error(format, *args):
    logging.getLogger().error(format, *args)


def parse_image_name(image_name: str):
    """
    Parses image_name and returns three values:
    1) The registry hostname/IP and and optional port, or None if not included.
    2) The repository path
    3) The image tag, or None if not included

    If parsing fails, an exception is raised
    """
    match = re.match(
        r"^(?:(?:(?P<registry>[^\/]+\.[^\/]+)\/)?(?P<repository>[^:]+))(?::(?P<tag>[^:]+))?$",
        image_name,
    )
    if match:
        return match.group("registry"), match.group("repository"), match.group("tag")
    raise ValueError("Could not parse image name %s", image_name)


def tagless_image_name(image_name: str) -> str:
    """
    Returns a string representing 'image_name' without the tag.
    """

    (registry, repo, tag) = parse_image_name(image_name)

    if registry:
        return f"{registry}/{repo}"
    else:
        return repo


def pairwise(iterable):
    """
    Yield successive overlapping pairs taken from the input iterable.

    Note this exists in itertools in Python 3.10. Once we're using at least
    Debian bookworm we can replace uses of this function with the itertools
    one.
    """
    iterator = iter(iterable)
    a = next(iterator, None)
    for b in iterator:
        yield a, b
        a = b
