import gitlab
import os


__client__ = None


def client() -> gitlab.Gitlab:
    """
    Returns a new GitLab client if the API is available in this environment.
    """
    global __client__

    if __client__ is not None:
        return __client__

    url = os.getenv("CI_SERVER_URL")

    if url is not None:
        __client__ = gitlab.Gitlab(url, job_token=os.getenv("CI_JOB_TOKEN"))

    return __client__
